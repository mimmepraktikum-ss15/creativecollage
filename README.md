# README #

1. Und die Anwendung zu öffnen, klonen Sie das Depository in Sourcetree oder laden Sie es herunter. 
2. Öffnen Sie dann im Ordner "creativecollage" die Datei "index.html" 
(Vorzugsweise mit dem Chrome Browser, für andere Browser kann derzeit keine vollständige Unterstützung garantiert werden.)
3. Es sind keine weiteren Dateien nötig

### Original Contributors ###

* Isabell Meyer
* Anna Walberer