var App = App || {};

App.CanvasController = function (canv) {

    "use strict";
    /* eslint-env browser,jquery,fabric,filereader,download */

    var img,
        downloadButton,
        imageRemove,
        imgInstance,
        saveCanvasButton,
        loadCanvasButton,
        activebefore,

        text,
        customText,
        textAdd,
        active;

    //saves current canvas in local storage
    //source: http://www.asquestion.com/question/32313034393234303334
    function saveCanvas() {
        var json = JSON.stringify(canv.toJSON());
        localStorage.setItem("savedImg", json);
    }

    //loads latest canvas from local storage
    //source: http://www.asquestion.com/question/32313034393234303334
    function loadCanvas() {
        var json = localStorage.getItem("savedImg");
        canv.loadFromJSON(json, function () {
            canv.renderAll();
        });
    }

    // get image from file
    function getImage(file, callback) {
        // create empty image object
        img = new Image();
        // set onload callback: call callback when image is loaded, bind created image to callback
        img.onload = callback.bind(this, img);
        // create object url from file and set as src for image
        img.src = URL.createObjectURL(file);
    }

    // callback for file drop
    function onFileLoaded(event, file) {
        // create image from "uploaded" file, first param is file, second param is callback function for when image is created
        getImage(file, function (img) {
            // creates fabric image, places it on canvas and reduces size
            imgInstance = new fabric.Image(img, {left: 100, top: 100});
            imgInstance.scale(0.5);
            // add image to canvas via fabric
            canv.add(imgInstance).renderAll();
            canv.setActiveObject(imgInstance);

            var imageTag = document.createElement("img"),
				imageContainer;
            imageTag.src = img.src;
            imageTag.style.display = "none";
            imageTag.id = "bild1";
            imageContainer = document.getElementById("imagecontainer");
            //document.body.appendChild(imageTag);
            imageContainer.appendChild(imageTag);
        });
    }

    // init upload via FileReaderJS (Wrapper library for HTML5 FileApi)
    function initUpload() {
        // options for drop target
        var options = {
            // set events
            on: {
                // callback for load event - fired when file is dropped on target
                load: onFileLoaded
            }
        };
        // set #dropzone-div as drop target
        FileReaderJS.setupDrop(document.getElementById("dropzone"), options);
    }

    function getImgInstance() {
        return imgInstance;
    }

    //removes currently adressed object
    function handleRemove() {
        canv.remove(canv.getActiveObject());
    }

    //downloads finished collage
    function onDownloadClicked() {
        var currentActive = new fabric.Path(),
			data;
        canv.setActiveObject(currentActive);
        data = canv.toDataURL("image/png");
        download(data, "myCollage", "image/png");
    }

    //removes filters for picture editing
    function onMouseUpRemove() {
        activebefore.filters = [];
        activebefore.applyFilters(canv.renderAll.bind(canv));
    }

    // adds a new custom text to the canvas
    function addText() {
        customText = document.querySelector("#customText").value;
        text = new fabric.Text(customText, { left: 80, top: 80, fontSize: 50, fontFamily: "Roboto" });
        canv.add(text);
        canv.renderAll();
        canv.setActiveObject(text);
    }

    //hides and shows editing properties if object is adressed
    function showFilters() {
        $(".filters").show();
    }

    function hideFilters() {
        $(".filters").hide();
    }

    function showTextStyles() {
        $(".textwrapper").show();
    }

    function hideTextStyles() {
        $(".textwrapper").hide();
    }

    function showTextColors() {
        $(".textcolor").show();
    }

    function hideTextColors() {
        $(".textcolor").hide();
    }

    //Filter
    function onMouseUpGrayscale() {
        activebefore.filters.push(new fabric.Image.filters.Grayscale());
        activebefore.applyFilters(canv.renderAll.bind(canv));
        canv.setActiveObject(activebefore);
    }

    function onMouseUpSepia() {
        activebefore.filters.push(new fabric.Image.filters.Sepia());
        activebefore.applyFilters(canv.renderAll.bind(canv));
        canv.setActiveObject(activebefore);
    }

    function onMouseUpInvert() {
        activebefore.filters.push(new fabric.Image.filters.Invert());
        activebefore.applyFilters(canv.renderAll.bind(canv));
        canv.setActiveObject(activebefore);
    }

    function onMouseUpEmboss() {
        activebefore.filters.push(new fabric.Image.filters.Convolute({
			matrix: [ 1, 1, 1,
                     1, 0.7, -1,
                     -1, -1, -1]
		}));
        activebefore.applyFilters(canv.renderAll.bind(canv));
        canv.setActiveObject(activebefore);
    }

    function onMouseUpSharpen() {
        activebefore.filters.push(new fabric.Image.filters.Convolute({
			matrix: [ 0, -1, 0,
                     -1, 5, -1,
                     0, -1, 0]
		}));
        activebefore.applyFilters(canv.renderAll.bind(canv));
        canv.setActiveObject(activebefore);
    }

    //Show Buttons on Image for Filters
    function showGrayButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpGrayscale);
        }
    }

    function showSepiaButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top + 25;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpSepia);
        }
    }

    function showInvertButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top + 50;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpInvert);
        }
    }

    function showEmbossButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top + 75;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpEmboss);
        }
    }

    function showSharpenButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top + 100;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpSharpen);
        }
    }

	function showRemoveButtonforFiltersOnImage(img) {
        active = canv.getActiveObject();
        if (active.get("type") === "image") {
            img.top = active.top + 130;
            img.left = active.left;
            canv.add(img);
            img.hasControls = img.hasBorders = img.selectable = false;
            canv.renderAll();

            img.on("mouseup", onMouseUpRemove);
        }
    }


	function removeEditingButtonsOnMoveAndSelectionCleared(img) {
		canv.on("object:moving", function () {
			img.visible = false;
		});
	}

	//Show buttons under texts for text editing
	function showBoldButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("fontWeight", "bold");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showUndoBoldButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 16;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("fontWeight", "normal");
				canv.setActiveObject(activebefore);
				canv.renderAll();
            });
		}
	}

	function showItalicButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 34;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("fontStyle", "italic");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showUndoItalicButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 50;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();

			text.on("mouseup", function () {
				activebefore.set("fontStyle", "normal");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showUnderlineButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 66;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("textDecoration", "underline");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showUndoUnderlineButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 82;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("textDecoration", "none");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showLinethroughButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 100;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("textDecoration", "line-through");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showUndoLinethroughButtonForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 70;
			text.left = active.left + 116;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.set("textDecoration", "normal");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	//add different text colors
	function showRedForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.setColor("#e40726");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showBlueForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left + 17;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.setColor("#1f3ceb");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showYellowForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left + 34;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function() {
				activebefore.setColor("#f8f244");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showGreenForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left + 51;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function() {
				activebefore.setColor("#278e38");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showWhiteForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left + 68;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.setColor("#ffffff");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function showBlackForTextEditing(text) {
		active = canv.getActiveObject();
		if (active.get("type") === "text") {
			text.top = active.top + 88;
			text.left = active.left + 85;
			canv.add(text);
			text.hasControls = text.hasBorders = text.selectable = false;
			canv.renderAll();
			text.on("mouseup", function () {
				activebefore.setColor("#000000");
				canv.setActiveObject(activebefore);
				canv.renderAll();
			});
		}
	}

	function removeEditingButtonsTextOnMoveAndSelectionCleared(text) {
		canv.on("object:moving", function () {
			text.visible = false;
		});
	}

    // adds properties to remove texts or images, save and load current collage and download finished collage
    function init() {
        initUpload();

        imageRemove = document.getElementById("imageRemove");
        imageRemove.addEventListener("click", handleRemove);

        downloadButton = document.getElementById("download");
        downloadButton.addEventListener("click", onDownloadClicked);

        saveCanvasButton = document.getElementById("saveCanvas");
        saveCanvasButton.addEventListener("click", saveCanvas);

        loadCanvasButton = document.getElementById("loadCanvas");
        loadCanvasButton.addEventListener("click", loadCanvas);

        //adds filter for Editing Controller and hide & show of text and image editing buttons
        canv.on({
            "object:selected": function (e) {
                showFilters();
                showTextStyles();
                showTextColors();
                canv.on("before:selection:cleared", function () {
                    activebefore = canv.getActiveObject();
                });

                //on image selected buttons for filters appear
                var grayscaleInCanvas = fabric.Image.fromURL("./res/buttons/grays.png", function(img) {
                    showGrayButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });
                var sepiaInCanvas = fabric.Image.fromURL("./res/buttons/sepia.png", function(img) {
                    showSepiaButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });
                var invertInCanvas = fabric.Image.fromURL("./res/buttons/invert.png", function(img) {
                    showInvertButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });
                var embossInCanvas = fabric.Image.fromURL("./res/buttons/emboss.png", function(img) {
                    showEmbossButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });
                var sharpenInCanvas = fabric.Image.fromURL("./res/buttons/sharpen.png", function(img) {
                    showSharpenButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });
				var removeFilterInCanvas = fabric.Image.fromURL("./res/buttons/undo.png", function(img) {
                    showRemoveButtonforFiltersOnImage(img);
                    removeEditingButtonsOnMoveAndSelectionCleared(img);
                });

				// on text selected buttons for text-editing appear
				// textstyles
				var bold = fabric.Image.fromURL("./res/buttons/bold.png", function(text) {
                    showBoldButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var undoBold = fabric.Image.fromURL("./res/buttons/undo.png", function(text) {
                    showUndoBoldButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var italic = fabric.Image.fromURL("./res/buttons/italic.png", function(text) {
					showItalicButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var undoItalic = fabric.Image.fromURL("./res/buttons/undo.png", function(text) {
                    showUndoItalicButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var underlined = fabric.Image.fromURL("./res/buttons/underline.png", function(text) {
					showUnderlineButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var undoUnderlined = fabric.Image.fromURL("./res/buttons/undo.png", function(text) {
                    showUndoUnderlineButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var linethrough = fabric.Image.fromURL("./res/buttons/strokethrough.png", function(text) {
					showLinethroughButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var undoLinethrough = fabric.Image.fromURL("./res/buttons/undo.png", function(text) {
                    showUndoLinethroughButtonForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });

				//changes colors
				var textRed = fabric.Image.fromURL("./res/buttons/red.png", function(text) {
                    showRedForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var textBlue = fabric.Image.fromURL("./res/buttons/blue.png", function(text) {
                    showBlueForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var textYellow = fabric.Image.fromURL("./res/buttons/yellow.png", function(text) {
                    showYellowForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var textGreen = fabric.Image.fromURL("./res/buttons/green.png", function(text) {
                    showGreenForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var textWhite = fabric.Image.fromURL("./res/buttons/white.png", function(text) {
                    showWhiteForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });
				var textBlack = fabric.Image.fromURL("./res/buttons/black.png", function(text) {
                    showBlackForTextEditing(text);
					removeEditingButtonsTextOnMoveAndSelectionCleared(text);
                });

                canv.renderAll();

            },
            "selection:cleared": function(e) {
                hideFilters();
                hideTextStyles();
                hideTextColors();
            },

			//last active object on top
            "object:moving": function(e) {
                e.target.bringToFront();
            }

		});


        //adds texts
        textAdd = document.getElementById("customText");
        textAdd.addEventListener("keydown", function(event) {
            if (event.keyCode === 13) {
                addText();
            }
        });

    }

        return {
        init: init,
        getImgInstance: getImgInstance
    };
};
