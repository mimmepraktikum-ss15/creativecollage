var App = App || {};

App.CreativeCollage = (function () {
    "use strict";

    /* eslint-env browser, jquery, fabric, filereader */

    var fabcanvas,
        canvas;

    function initFabric() {
        fabcanvas = new fabric.Canvas("canvas");
    }

    function init() {
        initFabric();
        canvas = new App.CanvasController(fabcanvas);
        canvas.init();
    }

    return {
        init: init,
    };
}());
